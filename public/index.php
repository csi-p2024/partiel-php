<?php
declare(strict_types=1);
require_once '../config/localConfig.php';
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <?php include_once 'inc/head.php'; ?>
    </head>
    <body>
        <?php include_once 'inc/header.php'; ?>

        <main>
            <article>
                <header>
                    <h1>Bienvenu au partiel de PHP!</h1>
                </header>
                <p>Le menu vous donne accès aux pages à réaliser.</p>
            </article>
        </main>

        <?php include_once 'inc/footer.php'; ?>
    </body>
</html>

