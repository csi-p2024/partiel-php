<?php

declare(strict_types=1);

require_once '../config/localConfig.php';

use Api\Service;

//  Récupération de l'url et séparation selon les /
//  Exemple d'URL:http://cni.phaln.info/services/api.obj.cni-uwpdemo.php/Mesures/Capteur/4
$requestUri = filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_SPECIAL_CHARS);

//  Séparation selon les '/'
$urlTab = explode('/', $requestUri);

//  Récupération du nom du script et explosion selon les /
$scriptName = filter_input(INPUT_SERVER, 'SCRIPT_NAME', FILTER_SANITIZE_SPECIAL_CHARS);

$scriptNameTab = explode('/', $scriptName);

//  On fait la différence de l'url - le nom du script pour isoler les paramètres
$tabDif = array_diff($urlTab, $scriptNameTab);

//  Récupération du service demandé
$serviceFunction = (count($tabDif) > 0) ? array_shift($tabDif) : null;

//  Positionne l'en-tête pour une réponse en json
if (!DUMP)
    header('Content-Type: application/json; charset=UTF-8');

//  Exécute la fonction de service demandée.
if ($serviceFunction != NULL) {
    try {
        //  Instanciation du service
        $service = new Service();
        if(DUMP) dump($service);

        //  Vérification de l'existance de la méthode demandée
        if (!method_exists($service, $serviceFunction))
            throw new Exception("fonction inexistante.");

        $res = $service->$serviceFunction($tabDif);
        echo $res;
    } catch (Throwable $e) {
        if(DUMP) dump($e);
        echo'{"Erreur":"ServiceFunction"}';
    }
} else {
    echo'{"msg":"Hello from CSI"}';
}