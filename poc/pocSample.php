<?php

//  Les deux lignes suivantes sont à inclure dans toutes vos pages "exécutables": 
//  les pages, formulaires, traitements de formulaires, pocs, tests unitaire, ...
//  ATTENTION: pas dans les classes ou les fichier inclus...
declare(strict_types=1);
require_once '../config/localConfig.php';

// Pensez aux use nécessaires

//  Utilisez un bloc try pour intercepter les erreurs et exceptions
try {
    $res = "Resultat";
    dump($res);
    
} catch (Throwable $ex) {
    echo'<p>Une erreur ou une exception a été lancée et non traitée...</p>';
    dump($ex->getMessage());
    dump($ex);
}