<?php

//  Les deux lignes suivantes sont à inclure dans toutes vos pages "exécutables": 
//  les pages, formulaires, traitements de formulaires, pocs, tests unitaire, ...
//  ATTENTION: pas dans les classes ou les fichier inclus...
declare(strict_types=1);
require_once '../config/localConfig.php';


use Entities\Nuisance;
use Repositories\LocalisationRepository;

//  Utilisez un bloc try pour intercepter les erreurs et exceptions
try {
    $rep = new LocalisationRepository();
    dump_var($rep, true, "Repo:");
    
    $obj = $rep->getById(1);
    dump_var($obj, true, "Coord:");
    
    $obj = $rep->getById(3);
    dump_var($obj, true, "Adr:");
       
    $obj = $rep->getById(99);
    dump_var($obj, true, "Rien...:");
    
    
    $nuis = new Nuisance(['fk_localisation'=>1]);
    dump($nuis->getLocalisation());
    dump_var($nuis, true, "Nuis loc 1");
    
    $nuis = new Nuisance(['fk_localisation'=>3]);
    dump($nuis->getLocalisation());
    dump_var($nuis, true, "Nuis loc 3");
    
} catch (Throwable $ex) {
    echo'<p>Une erreur ou une exception a été lancée et non traitée...</p>';
    dump($ex->getMessage());
    dump($ex);
}