<!DOCTYPE html>
<?php
require_once '../config/prepaConfig.php';
$dy = ($hr==='965d5ffc4f95ac9ba2e5675ebda36065c6bfbf36' && $hf==='cfae8b12bb5d2c7b439be43d386fa1d802eba857') ? 'ok' : 'ko';
$ntest = 0;
?>
<html>
    <head>
        <meta charset="utf-8">
	<link rel="stylesheet" href="css/styles.css" />
        <title>Préparation</title>
    </head>
    <body class="<?= $dy ?>">
	<h1>Evaluation PHP</h1>
	<!-- ---------------------------------------------------------------- -->

	<p class="question"><img class='imgAttendu' style='max-width: 40%;' src="img/img1.png"/>
	    <?= ++$ntest ?>. Créez un tableau vide $tab1.<br/>
		Ajoutez-lui les clés 'idLocalisation', 'adresse', 'codePostal' et 'commune'
                avec pour valeurs respectives: </p> <pre>'1', '133 rue Marius Berliet', 69008, 'Lyon'</pre>
	<p class="question">Affichez-le simplement pour vérifier.</p>
	<?php
	//  ---> A COMPLETER



	//  ---> FIN
	?>
	<?= $v($tab1, $ntest) ?>
	<!-- ---------------------------------------------------------------- -->

	<p class="question"><img class='imgAttendu' style='max-width: 40%;' src="img/img2.png"/>
	    <?= ++$ntest ?>. 2. Créez un deuxième tableau associatif $tab2 du même format
            avec les valeurs</p><pre> '3', '37 rue Saint Nestor',69008,'Lyon'</pre>
	<p class="question">Affichez-le simplement pour vérifier.</p>
	
	<?php
	//  ---> A COMPLETER



	//  ---> FIN
	?>
	<?= $v($tab2, $ntest) ?>
	<!-- ---------------------------------------------------------------- -->

	<p class="question"><img class='imgAttendu' style='max-width: 40%;' src="img/img3.png"/>
	    <?= ++$ntest ?>. Créez un tableau vide nommé $tab3.<br/>
		Ajoutez-lui comme éléments les variables $tab1 et $tab2.</p>
	<p class="question">Affichez-le simplement pour vérifier.</p>
	<?php
	//  ---> A COMPLETER en PHP



	//  ---> FIN
	?>
	<?= $v($tab3, $ntest) ?>
	<!-- ---------------------------------------------------------------- -->

	<p class="question"><img class='imgAttendu' style='max-width: 30%;' src="img/img4.png"/>
	    <?= ++$ntest ?>. Affichez le contenu de $tab3 dans une table html, 
		la première ligne de titre contenant les clés.<br/>
		--> Valeurs attendues à confirmer visuellement :</p>
	<?php
	//  ---> SOIT A COMPLETER EN PHP



	//  ---> FIN
	?>
	<!--  SOIT A COMPLETER en HTML -->


	
	<!--  FIN -->

        <!-- ---------------------------------------------------------------- -->

	<p class="question"><img class='imgAttendu' style='max-width: 20%;' src="img/img5.png"/>
	    <?= ++$ntest ?>. Connectez-vous, en PDO, dans une variable nommée $db, à la base de données,
	    en utilisant directement vos données de connexion $infoBdd située dans le fichier "/config/localConfig.php".<br/>
	    Pensez à prendre en charge le charset.</p>
	<p class="question">Affichez simplement $db->errorInfo() pour vérifier l'absence d'erreur.</p>
	<?php
        try{
	//  ---> A COMPLETER



	//  ---> FIN
        } catch(Throwable $e)
        {
            echo'<p class="bad">$db incorrecte: '.$e->getMessage().'</p>';        }
	?>
        <?php
        if (isset($db)):
            $err = $db->errorInfo();
            $bdd = isset($err) ? md5(json_encode($err)) : '';
            echo $v($bdd, $ntest);
	else:
            echo'<p class="bad">$db incorrecte</p>';
        endif;
        ?>
        <!-- ---------------------------------------------------------------- -->

	<p class="question"><img class='imgAttendu' style='max-width: 35%;' src="img/img6.png"/>
	    <?= ++$ntest ?>. Exécutez une requête préparée SQL récupérant (dans cet ordre) les codes postaux et communes de la table 'adresse'.<br/> 
            Le résultats de l'exécution est rangé dans une variable nommée $res,</p>
	<p class="question">Affichez-le simplement pour vérifier.</p>
	<?php
        try{
	//  ---> A COMPLETER en PHP



	//  ---> FIN
        } catch(Throwable $e)
        {
            echo'<p class="bad">$db ou requete incorrecte: '.$e->getMessage().'</p>';        }
	?>
	<?= $v([$reqPrep,$res], $ntest) ?>
	<!-- ---------------------------------------------------------------- -->

	<p class="question"><img class='imgAttendu' style='max-width: 35%;' src="img/img7.png"/>
	    <?= ++$ntest ?>. Rangez tous les résultats dans un tableau d'objet nommé $lieux.<br/>
		Affichez simplement ce tableau.<br/>
		--> Extrait des valeurs attendues: </p>
	<?php
	//  ---> A COMPLETER en PHP



	//  ---> FIN
	?>
	<?= $v($lieux, $ntest) ?>
	<!-- ---------------------------------------------------------------- -->

	<p class="question"><img class='imgAttendu' style='max-width: 30%;' src="img/img8.png"/>
	    <?= ++$ntest ?>. Affichez le contenu de $lieux dans une table html, 
		la première ligne de titre contenant les clés.<br/>
		--> Valeurs attendues à confirmer visuellement :</p>
	<?php
	//  ---> SOIT A COMPLETER EN PHP



	//  ---> FIN
	?>
	<!--  SOIT A COMPLETER en HTML -->


	
	<!--  FIN -->

        <!-- ---------------------------------------------------------------- -->

	<!-- ---------------------------------------------------------------- -->
	<p class="validation">FAITES VALIDER</p>
	<!-- ---------------------------------------------------------------- -->
    </body>
</html>