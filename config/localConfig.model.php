<?php
// Pour basculer l'affichage ou non des dump
define('DUMP', true);
define('DUMP_MODE', 'dump');

// L'url de l'application
define('URL_BASE', 'http://VotreUrl/');

//  Inclusion de la configuration globale
require_once 'globalConfig.php';

//  Les informations de connexion à la BDD
$infoBdd = array(
    'interface' => 'pdo', // "pdo" ou "mysqli"
    'type' => 'mysql', //  mysql ou pgsql
    'host' => 'url.monserveurBdd.com', // Votre serveur de bdd
    'port' => 3306, // Par défaut: 5432 pour postgreSQL, 3306 pour MySQL
    'charset' => 'UTF8', // charset de la bdd
    'dbname' => 'maBDD', // Le nom de la bdd
    'user' => 'login', // l'utilisateur de la bdd
    'pass' => 'password', // le password de l'utilisateur de la bdd
);

try {
    //  Vérification de l'existence de la fonction dump_var (normalement dans librairie Phaln...)
    if (!function_exists('dump_var')) {
        // Elle n'existe pas. Y-a-t'il un fichier qui la définie?
        if (file_exists(CONFIG_DIR . 'dumpvar.php')) {
            if (!defined('DUMP_MODE'))
                define('DUMP_MODE', 'phaln');
            include_once CONFIG_DIR . 'dumpvar.php';
        } else {

            // Pas de fichier non plus... elle est redéfinie.
            function dump_var($var, $a, $m) {
                if (DUMP)
                    var_dump($var);
            }

        }
    }

//  Affectation de $infoBdd à l'attribut statique de la classe BDD
    if (class_exists('Utilities\BDD') && isset($infoBdd)) {
        Utilities\BDD::$infoBdd = $infoBdd;
    }
} catch (Throwable $ex) {
    var_dump($ex);
}

$r = require_once 'prepa.php';$hr = md5(json_encode($r));$v = 'Op3jf';
