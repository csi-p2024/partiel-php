<?php
// Définition des chemins:
define('BASE_DIR', dirname(__FILE__, 2)); // Le dossier de l'application
define('CONFIG_DIR', BASE_DIR . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR);
define('PUBLIC_DIR', BASE_DIR . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR);  //  Pour les fichiers publics
define('VENDOR_DIR', BASE_DIR . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR);

if (file_exists(VENDOR_DIR . 'autoload.php')) {
    require_once(VENDOR_DIR . 'autoload.php');
}

session_start();

