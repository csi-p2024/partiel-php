<?php

declare(strict_types=1);
require_once 'config/localConfig.php';

use PHPUnit\Framework\TestCase;
use Entities\Adresse;

class AdresseTest extends TestCase {
    
    /**
     * Teste le constructeur avec paramètres OK et les Getters
     */
    public function testConstructFullOk() {
        $datas = [
            'idLocalisation' => 1,
            'adresse' => '133 rue Marius Berliet',
            'codePostal' => 69008,
            'commune' => 'Lyon',
        ];
        $expected = [
            'idLocalisation' => 1,
            'adresse' => '133 rue Marius Berliet',
            'codePostal' => 69008,
            'commune' => 'Lyon',
        ];
        
        $entity = new Adresse($datas);
        $this->assertSame($expected['idLocalisation'], $entity->getIdLocalisation());
        $this->assertSame($expected['adresse'], $entity->getAdresse());
        $this->assertSame($expected['codePostal'], $entity->getCodePostal());
        $this->assertSame($expected['commune'], $entity->getCommune());
    }
    
    /**
     * Teste le constructeur avec paramètres OK sans idLocalisation
     */
    public function testConstructSansIdLocalisation() {
        $datas = [
            'adresse' => '133 rue Marius Berliet',
            'codePostal' => 69008,
            'commune' => 'Lyon',
        ];
        $expected = [
            'idLocalisation' => null,
            'adresse' => '133 rue Marius Berliet',
            'codePostal' => 69008,
            'commune' => 'Lyon',
        ];
        
        $entity = new Adresse($datas);
        $this->assertSame($expected['idLocalisation'], $entity->getIdLocalisation());
        $this->assertSame($expected['adresse'], $entity->getAdresse());
        $this->assertSame($expected['codePostal'], $entity->getCodePostal());
        $this->assertSame($expected['commune'], $entity->getCommune());
    }

    
    /**
     * Teste les Setters (affectation et type de retour) avec des paramètres OK
     */
    public function testSettersOk() {
        $datas = [
            'adresse' => '133 rue Marius Berliet',
            'codePostal' => 69008,
            'commune' => 'Lyon',
        ];
        $modif = [
            'idLocalisation' => 1,
            'adresse' => '1 place de la mairie',
            'codePostal' => 39140,
            'commune' => 'Bletterans',
        ];
        $expected = [
            'idLocalisation' => 1,
            'adresse' => '1 place de la mairie',
            'codePostal' => 39140,
            'commune' => 'Bletterans',
        ];
        
        $entity = new Adresse($datas);
        
        $entMod = $entity->setIdLocalisation($modif['idLocalisation']);
        $this->assertInstanceOf(Adresse::class, $entMod);       
        $this->assertSame($expected['idLocalisation'], $entMod->getIdLocalisation());
        
        $entMod = $entity->setAdresse($modif['adresse']);
        $this->assertInstanceOf(Adresse::class, $entMod);       
        $this->assertSame($expected['adresse'], $entity->getAdresse());
        
        $entMod = $entity->setCodePostal($modif['codePostal']);
        $this->assertInstanceOf(Adresse::class, $entMod);       
        $this->assertSame($expected['codePostal'], $entity->getCodePostal());
        
        $entMod = $entity->setCommune($modif['commune']);
        $this->assertInstanceOf(Adresse::class, $entMod);       
        $this->assertSame($expected['commune'], $entity->getCommune());
    }
    
    /**
     * Teste la troncature dans setAdresse et setCommune
     */
    public function testSettersTroncatureAdresseCommune() {
        $datas = [
            'adresse' => '133 rue Marius Berliet',
            'codePostal' => 69008,
            'commune' => 'Lyon',
        ];
        $modif = [
            'adresse' => 'azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN',
            'commune' => 'azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN',
        ];
        $expected = [
            'adresse' => 'azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCV',
            'commune' => 'azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCV',
        ];
        
        $entity = new Adresse($datas);
        
        $entMod = $entity->setAdresse($modif['adresse']);
        $this->assertInstanceOf(Adresse::class, $entMod);       
        $this->assertSame($expected['adresse'], $entity->getAdresse());
        
        $entMod = $entity->setCommune($modif['commune']);
        $this->assertInstanceOf(Adresse::class, $entMod);       
        $this->assertSame($expected['commune'], $entity->getCommune());
    }
    
    /**
     * Teste setIdLocalisation après mauvais type
     */
    public function testSetIdLocalisationBadType() {
        $this->expectException(\TypeError::class);
        
        $datas = [
            'idLocalisation' => 1,
            'adresse' => '133 rue Marius Berliet',
            'codePostal' => 69008,
            'commune' => 'Lyon',
        ];
        
        $entity = new Adresse($datas);
        $entity->setIdLocalisation(['bad']);
    }
    
    /**
     * Teste setAdresse après mauvais type
     */
    public function testSetAdresseBadType() {
        $this->expectException(\TypeError::class);
        
        $datas = [
            'idLocalisation' => 1,
            'adresse' => '133 rue Marius Berliet',
            'codePostal' => 69008,
            'commune' => 'Lyon',
        ];
        
        $entity = new Adresse($datas);
        $entity->setAdresse(['bad']);
    }
    
    /**
     * Teste setAdresse chaine vide
     */
    public function testSetAdresseEmptyString() {
        $this->expectException(\TypeError::class);
        
        $datas = [
            'idLocalisation' => 1,
            'adresse' => '133 rue Marius Berliet',
            'codePostal' => 69008,
            'commune' => 'Lyon',
        ];
        
        $entity = new Adresse($datas);
        $entity->setAdresse('');
    }
    
    /**
     * Teste setCodePostal avec mauvais type
     */
    public function testSetCodePostalBadType() {
        $this->expectException(\TypeError::class);
        
        $datas = [
            'idLocalisation' => 1,
            'adresse' => '133 rue Marius Berliet',
            'codePostal' => 69008,
            'commune' => 'Lyon',
        ];
        
        $entity = new Adresse($datas);
        $entity->setCodePostal('bad');
    }
    
    /**
     * Teste setCommune avec mauvais type
     */
    public function testSetCommune() {
        $this->expectException(\TypeError::class);
        
        $datas = [
            'idLocalisation' => 1,
            'adresse' => '133 rue Marius Berliet',
            'codePostal' => 69008,
            'commune' => 'Lyon',
        ];
        
        $entity = new Adresse($datas);
        $entity->setCommune(['bad']);
    }
    
    /**
     * Teste setCommune avec chaine vide
     */
    public function testSetCommuneEmptyString() {
        $this->expectException(\TypeError::class);
        
        $datas = [
            'idLocalisation' => 1,
            'adresse' => '133 rue Marius Berliet',
            'codePostal' => 69008,
            'commune' => 'Lyon',
        ];
        
        $entity = new Adresse($datas);
        $entity->setCommune('');
    }
    
    /**
     * Teste le constructeur sans paramètre
     * Lance une erreur TypeError
     */
    public function testConstructEmpty() {
        $this->expectException(\TypeError::class);

        $entity = new Adresse();
    }
    
    /**
     * Teste le constructeur avec paramètres incomplets (sans adresse)
     * Lance une erreur TypeError
     */
    public function testConstructWithoutAdresse() {
        $this->expectException(\TypeError::class);

        $datas = [
            'idLocalisation' => 1,
            'codePostal' => 69008,
            'commune' => 'Lyon',
        ];
        
        $entity = new Adresse($datas);
    }    
}
