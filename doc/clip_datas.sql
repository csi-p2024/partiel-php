-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mar. 19 déc. 2023 à 22:32
-- Version du serveur : 5.7.36
-- Version de PHP : 8.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `csi_clip`
--

--
-- Déchargement des données de la table `localisation`
--

INSERT INTO `localisation` (`idLocalisation`, `genreLocalisation`) VALUES
(1, 'Coordonnees'),
(2, 'Coordonnees'),
(3, 'Adresse'),
(4, 'Coordonnees'),
(5, 'Adresse'),
(6, 'Coordonnees'),
(7, 'Adresse'),
(8, 'Coordonnees'),
(9, 'Coordonnees'),
(10, 'Adresse'),
(11, 'Adresse'),
(12, 'Adresse'),
(13, 'Coordonnees'),
(14, 'Adresse'),
(15, 'Coordonnees'),
(16, 'Adresse'),
(17, 'Coordonnees'),
(18, 'Coordonnees');


--
-- Déchargement des données de la table `adresse`
--

INSERT INTO `adresse` (`idLocalisation`, `adresse`, `codePostal`, `commune`) VALUES
(3, '123 rue devant', 39140, 'FONTAINEBRUX'),
(5, '3 rue de l\'eau', 39140, 'LARNAUD'),
(7, '517 rue des Bordes', 71580, 'SAILLENARD'),
(10, '782 rue derrière', 39140, 'FONTAINEBRUX'),
(11, 'Chemin du centre', 39140, 'LES REPOTS'),
(12, '600 chemin du Haut Chemin', 39570, 'COURLAOUX'),
(14, '120 Rue du 19 Mars 1962', 71580, 'BEAUREPAIRE EN BRESSE'),
(16, 'Chemin du centre', 39140, 'LES REPOTS');

--
-- Déchargement des données de la table `coordonnees`
--

INSERT INTO `coordonnees` (`idLocalisation`, `latitude`, `longitude`, `commentaires`) VALUES
(1, 46.6879, 5.42865, 'Centre SYDOM'),
(2, 46.6946, 5.41408, 'Etang'),
(4, 46.6986, 5.40843, 'Pont du Jeanbon'),
(6, 46.6952, 5.41478, 'Etang'),
(8, 46.7047, 5.41738, 'Lavoir'),
(9, 46.7098, 5.42844, 'Alambic'),
(13, 46.6986, 5.40843, 'Pont du Jeanbon'),
(15, 46.7143, 5.41817, 'Serenne'),
(17, 46.6989, 5.41399, 'Etang du Prince'),
(18, 46.7055, 5.41158, 'Pèse lait');

--
-- Déchargement des données de la table `declarant`
--

INSERT INTO `declarant` (`idDeclarant`, `mail`, `nom`, `prenom`, `passwd`) VALUES
(1, 'c.honnete@gmail.com', 'HONNETE', 'Camille', '$2y$10$Nq1YubAW9kBeMCRdYJZdkOc7ZeOHi/sT9AMDejAw3PHBBvHcAN8su'),
(2, 'rudymantere@laposte.fr', 'MANTERE', 'Rudy', '$2y$10$dM3kCO37VkVo8yCssf8hYOPCsJUQ2d.ozK1VRwgvcDAiKnoY6Ia2G'),
(3, 'monde69@gmail.com', 'MONDE', 'Betty', '$2y$10$nbzPWDpg6IR3FX/GMu/a1utwbTMlcTBPvZtQ5ij2se3MVzJbICl5m'),
(4, 'zieuklair.b@phaln.info', 'ZIEUKLAIR', 'Bruno', '$2y$10$2Jv5czWQ5dT0CUGmhep2yuIt9TkP9l98isrKiF17tZR2DHMJws5vO');

--
-- Déchargement des données de la table `nature`
--

INSERT INTO `nature` (`idNature`, `denomination`) VALUES
(1, 'Bruit'),
(2, 'Odeur'),
(3, 'Fumées'),
(4, 'Efluants');

--
-- Déchargement des données de la table `nuisance`
--

INSERT INTO `nuisance` (`idNuisance`, `date`, `niveau`, `fk_nature`, `fk_declarant`, `fk_localisation`) VALUES
(1, '2023-10-09 14:03:53', 5, 2, 1, 1),
(2, '2023-10-09 15:03:53', 6, 2, 3, 2),
(3, '2020-02-09 07:30:40', 6, 3, 4, 3),
(4, '2020-02-09 08:28:38', 3, 4, 1, 4),
(5, '2020-02-09 20:30:40', 6, 3, 4, 5),
(6, '2020-02-12 20:31:41', 7, 3, 3, 6),
(7, '2020-02-12 20:32:40', 7, 3, 2, 7),
(8, '2020-02-12 20:33:00', 7, 3, 1, 8),
(9, '2020-02-20 07:30:40', 3, 2, 4, 9),
(10, '2020-02-20 08:30:40', 6, 2, 4, 10),
(11, '2020-02-20 08:45:40', 8, 2, 4, 11),
(12, '2020-02-20 17:45:40', 10, 2, 4, 12),
(13, '2020-02-20 18:45:40', 9, 2, 4, 13),
(14, '2020-02-21 07:20:40', 5, 2, 2, 14),
(15, '2020-02-21 08:20:40', 6, 2, 3, 15),
(16, '2020-02-22 08:25:40', 7, 2, 2, 16),
(17, '2020-02-24 17:25:40', 9, 2, 2, 17),
(18, '2020-02-24 18:25:40', 9, 2, 3, 18);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
