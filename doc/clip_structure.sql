-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mar. 19 déc. 2023 à 22:31
-- Version du serveur : 5.7.36
-- Version de PHP : 8.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `csi_clip`
--

-- --------------------------------------------------------

--
-- Structure de la table `adresse`
--

DROP TABLE IF EXISTS `adresse`;
CREATE TABLE IF NOT EXISTS `adresse` (
  `idLocalisation` int(11) NOT NULL COMMENT 'Aussi clé étrangère vers localisation',
  `adresse` varchar(50) NOT NULL COMMENT 'Le numéro et le nom de la rue',
  `codePostal` int(11) NOT NULL COMMENT 'Le code postal',
  `commune` varchar(50) NOT NULL COMMENT 'La ville',
  PRIMARY KEY (`idLocalisation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `coordonnees`
--

DROP TABLE IF EXISTS `coordonnees`;
CREATE TABLE IF NOT EXISTS `coordonnees` (
  `idLocalisation` int(11) NOT NULL COMMENT 'Aussi clé étrangère vers localisation',
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `commentaires` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idLocalisation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `declarant`
--

DROP TABLE IF EXISTS `declarant`;
CREATE TABLE IF NOT EXISTS `declarant` (
  `idDeclarant` int(11) NOT NULL AUTO_INCREMENT,
  `mail` varchar(45) NOT NULL,
  `nom` varchar(25) NOT NULL,
  `prenom` varchar(25) DEFAULT NULL,
  `passwd` varchar(68) NOT NULL COMMENT 'Par défaut, 123 avec password_hash',
  PRIMARY KEY (`idDeclarant`),
  UNIQUE KEY `mail` (`mail`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `localisation`
--

DROP TABLE IF EXISTS `localisation`;
CREATE TABLE IF NOT EXISTS `localisation` (
  `idLocalisation` int(11) NOT NULL AUTO_INCREMENT,
  `genreLocalisation` enum('Coordonnees','Adresse') NOT NULL DEFAULT 'Adresse' COMMENT 'Le type de localisation: coordonnées géographiques ou adresse postale',
  PRIMARY KEY (`idLocalisation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `nature`
--

DROP TABLE IF EXISTS `nature`;
CREATE TABLE IF NOT EXISTS `nature` (
  `idNature` int(11) NOT NULL AUTO_INCREMENT,
  `denomination` varchar(25) NOT NULL,
  PRIMARY KEY (`idNature`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `nuisance`
--

DROP TABLE IF EXISTS `nuisance`;
CREATE TABLE IF NOT EXISTS `nuisance` (
  `idNuisance` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL COMMENT 'Date de déclaration',
  `niveau` int(2) UNSIGNED NOT NULL COMMENT 'Niveau de 1 (faible) à 10 (insupportable)',
  `fk_nature` int(11) NOT NULL COMMENT 'Nature de la nuisance',
  `fk_declarant` int(11) NOT NULL COMMENT 'Auteur de la déclaration',
  `fk_localisation` int(11) NOT NULL COMMENT 'Localisation de la nuisance',
  PRIMARY KEY (`idNuisance`),
  KEY `fk_nature` (`fk_nature`),
  KEY `fk_declarant` (`fk_declarant`),
  KEY `fk_localisation` (`fk_localisation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `coordonnees`
--
ALTER TABLE `coordonnees`
  ADD CONSTRAINT `coordonnees_ibfk_1` FOREIGN KEY (`idLocalisation`) REFERENCES `localisation` (`idLocalisation`);

--
-- Contraintes pour la table `adresse`
--
ALTER TABLE `adresse`
  ADD CONSTRAINT `adresse_ibfk_1` FOREIGN KEY (`idLocalisation`) REFERENCES `localisation` (`idLocalisation`);

--
-- Contraintes pour la table `nuisance`
--
ALTER TABLE `nuisance`
  ADD CONSTRAINT `nuisance_ibfk_1` FOREIGN KEY (`fk_nature`) REFERENCES `nature` (`idNature`),
  ADD CONSTRAINT `nuisance_ibfk_2` FOREIGN KEY (`fk_declarant`) REFERENCES `declarant` (`idDeclarant`),
  ADD CONSTRAINT `nuisance_ibfk_3` FOREIGN KEY (`fk_localisation`) REFERENCES `localisation` (`idLocalisation`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
